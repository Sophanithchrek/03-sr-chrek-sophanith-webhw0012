import React from "react";
import {
    BrowserRouter,
    Switch,
    Route,
    Link,
    useParams,
    useRouteMatch
} from "react-router-dom";

function Video() {
    return (
        <BrowserRouter>
            <div className="container text-left" style={{ marginTop: "20px" }}>
                <ul>
                    <li>
                        <Link to="/Video/Animation">Animation</Link>
                    </li>
                    <li>
                        <Link to="/Video/Movie">Movie</Link>
                    </li>
                </ul>
                <Switch>
                    <Route path="/Video/Animation">
                        <Animation />
                    </Route>
                    <Route path="/Video/Movie">
                        <Movie />
                    </Route>
                </Switch>
            </div>
        </BrowserRouter>
    );
}

function Animation() {
    let { path, url } = useRouteMatch();
    return (
        <div>
            <h2>Animation Category</h2>
            <ul>
                <li>
                    <Link to={`${url}/Action`}>Action</Link>
                </li>
                <li>
                    <Link to={`${url}/Romance`}>Romance</Link>
                </li>
                <li>
                    <Link to={`${url}/Comedy`}>Comedy</Link>
                </li>
            </ul>

            <Switch>
                <Route exact path={path}>
                    <h3>Please Select A Topic.</h3>
                </Route>
                <Route path={`${path}/:animateCateId`}>
                    <AnimationCategory />
                </Route>
            </Switch>
        </div>
    );
}

function AnimationCategory() {
    let { animateCateId } = useParams();
    return (
        <div>
            <h3>{animateCateId}</h3>
        </div>
    );
}

function Movie() {
    let { path, url } = useRouteMatch();
    return (
        <div>
            <h2>Movie Category</h2>
            <ul>
                <li>
                    <Link to={`${url}/Adventure`}>Adventure</Link>
                </li>
                <li>
                    <Link to={`${url}/Comedy`}>Comedy</Link>
                </li>
                <li>
                    <Link to={`${url}/Crime`}>Crime</Link>
                </li>
                <li>
                    <Link to={`${url}/Documentary`}>Documentary</Link>
                </li>
            </ul>

            <Switch>
                <Route exact path={path}>
                    <h3>Please Select A Topic.</h3>
                </Route>
                <Route path={`${path}/:movieCategoryId`}>
                    <MovieCategory />
                </Route>
            </Switch>
        </div>
    );
}

function MovieCategory() {
    let { movieCategoryId } = useParams();
    return (
        <div>
            <h3>{movieCategoryId}</h3>
        </div>
    );
}

export default Video