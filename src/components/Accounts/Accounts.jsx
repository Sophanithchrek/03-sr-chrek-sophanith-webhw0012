import React from 'react'
import { BrowserRouter, Link, useLocation } from "react-router-dom"

function Account() {
    return (
        <BrowserRouter>
            <div className="container text-left">
                <AccountCategory />
            </div>
        </BrowserRouter>
    )
}

function useQuery() {
    return new URLSearchParams(useLocation().search);
}

function AccountCategory() {
    let query = useQuery();
    return (
        <div>
            <div>
                <h2>Accounts</h2>
                <ul>
                    <li>
                        <Link to="/Account?name=netflix">Netflix</Link>
                    </li>
                    <li>
                        <Link to="/Account?name=zillow-group">Zillow Group</Link>
                    </li>
                    <li>
                        <Link to="/Account?name=yahoo">Yahoo</Link>
                    </li>
                    <li>
                        <Link to="/Account?name=modus-create">Modus Create</Link>
                    </li>
                </ul>

                <Child name={query.get("name")} />
            </div>
        </div>
    );
}

function Child({ name }) {
    return (
        <div>
            {name ? (
                <h3>
                    The <code>name</code> in the query string is &quot;{name}
              &quot;
                </h3>
            ) : (
                    <h3>There is no name in the query string</h3>
                )}
        </div>
    );
}

export default Account