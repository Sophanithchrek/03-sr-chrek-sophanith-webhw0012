import React from 'react'

function View(props) {
    return (
        <div>
            <h3>This is content from post {props.match.params.id}</h3>
        </div>
    )
}

export default View
