import React from 'react'
import auth from './Authenticate'

function Auth(props) {
    return (
        <div className="container" style={{ marginTop: "20px" }}>
            <div className="row">
                <input type="text" name="" id="txtname" />
                <input type="password" name="" id="txtpass" />
                <button
                    onClick={() => {
                        auth.login(() => {
                            props.history.push("/Welcome");
                        });
                    }}
                    className="btn btn-primary"
                >Login</button>
            </div>
        </div>
    );
}

export default Auth









