class Authenticate {
  constructor() {
    this.authenticate = false;
  }

  login(callback) {
    this.authenticate = true;
    callback();
  }

  isAuthenticated() {
    return this.authenticate;
  }
}

export default new Authenticate();
