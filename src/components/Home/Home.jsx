import React from 'react'
import { CardDeck, Card, Container, Row, Col, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import "./Style.css";

function Home() {
    return (
        <div>
            <Container>
                <Row>
                    <Col>
                        <CardDeck>
                            <Card>
                                <Card.Img variant="top" src="https://cdn.vox-cdn.com/thumbor/c43RmSnhl_QchD3OiSf90L3tkak=/0x0:4282x2855/1200x800/filters:focal(1012x413:1696x1097)/cdn.vox-cdn.com/uploads/chorus_image/image/65768072/1184208109.jpg.0.jpg" />
                                <Card.Body>
                                    <Card.Title>Card title</Card.Title>
                                    <Card.Text>
                                        This is a wider card with supporting text below as a natural lead-in to
                                        additional content. This content is a little bit longer. <br/><br/> <Link to="/Post/1"><Button variant="primary">See More</Button></Link>
                                    </Card.Text>
                                </Card.Body>
                                <Card.Footer>
                                    <small className="text-muted">Last updated 3 mins ago</small>
                                </Card.Footer>
                            </Card>
                            <Card>
                                <Card.Img variant="top" src="https://cdn.vox-cdn.com/thumbor/oo2pkK2W8TaF1Mbj7aAglHQGLLA=/0x0:5472x3648/1200x800/filters:focal(1962x222:2836x1096)/cdn.vox-cdn.com/uploads/chorus_image/image/66879487/1193104957.jpg.0.jpg" />
                                <Card.Body>
                                    <Card.Title>Card title</Card.Title>
                                    <Card.Text>
                                        This card has supporting text below as a natural lead-in to additional
                                        content. <br/><br/> <Link to="/Post/2"><Button variant="primary">See More</Button></Link>
                                    </Card.Text>
                                </Card.Body>
                                <Card.Footer>
                                    <small className="text-muted">Last updated 3 mins ago</small>
                                </Card.Footer>
                            </Card>
                            <Card>
                                <Card.Img variant="top" src="https://cdn.vox-cdn.com/thumbor/XRZvIYRdKj9in_WK1lybK7e8J8s=/0x0:4780x3187/1200x800/filters:focal(1946x741:2710x1505)/cdn.vox-cdn.com/uploads/chorus_image/image/66876301/1235601086.jpg.0.jpg" />
                                <Card.Body>
                                    <Card.Title>Card title</Card.Title>
                                    <Card.Text>
                                        This is a wider card with supporting text below as a natural lead-in to
                                        additional content. This card has even longer content than the first to
                                        show that equal height action. <br/><br/> <Link to="/Post/3"><Button variant="primary">See More</Button></Link>
                                    </Card.Text>
                                </Card.Body>
                                <Card.Footer>
                                    <small className="text-muted">Last updated 3 mins ago</small>
                                </Card.Footer>
                            </Card>
                        </CardDeck>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <CardDeck>
                            <Card>
                                <Card.Img variant="top" src="https://www.thisisanfield.com/wp-content/uploads/P2020-02-18_Atletico_Liverpool-46.jpg" />
                                <Card.Body>
                                    <Card.Title>Card title</Card.Title>
                                    <Card.Text>
                                        This is a wider card with supporting text below as a natural lead-in to
                                        additional content. This content is a little bit longer. <br/><br/> <Link to="/Post/4"><Button variant="primary">See More</Button></Link>
                                    </Card.Text>
                                </Card.Body>
                                <Card.Footer>
                                    <small className="text-muted">Last updated 3 mins ago</small>
                                </Card.Footer>
                            </Card>
                            <Card>
                                <Card.Img variant="top" src="https://img.vavel.com/chelsea-bayern-1582667383615.jpg" />
                                <Card.Body>
                                    <Card.Title>Card title</Card.Title>
                                    <Card.Text>
                                        This card has supporting text below as a natural lead-in to additional
                                        content. <br/><br/> <Link to="/Post/5"><Button variant="primary">See More</Button></Link>
                                    </Card.Text>
                                </Card.Body>
                                <Card.Footer>
                                    <small className="text-muted">Last updated 3 mins ago</small>
                                </Card.Footer>
                            </Card>
                            <Card>
                                <Card.Img variant="top" src="https://cdn.i-scmp.com/sites/default/files/styles/1200x800/public/d8/images/methode/2019/12/29/2ed021a6-2a19-11ea-9939-941d1970c7f1_image_hires_170016.JPG?itok=DFkeXexK&v=1577610021" />
                                <Card.Body>
                                    <Card.Title>Card title</Card.Title>
                                    <Card.Text>
                                        This is a wider card with supporting text below as a natural lead-in to
                                        additional content. This card has even longer content than the first to
                                        show that equal height action. <br/><br/> <Link to="/Post/6"><Button variant="primary">See More</Button></Link>
                                    </Card.Text>
                                </Card.Body>
                                <Card.Footer>
                                    <small className="text-muted">Last updated 3 mins ago</small>
                                </Card.Footer>
                            </Card>
                        </CardDeck>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Home
