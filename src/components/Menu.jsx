import React from 'react'
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import { Link } from 'react-router-dom'

function Menu() {
    return (
        <div>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <div className="container">
                    <Navbar.Brand as={Link} to="/">React Router</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link as={Link} to="/Home">Home</Nav.Link>
                            <Nav.Link as={Link} to="/Video">Video</Nav.Link>
                            <Nav.Link as={Link} to="/Account">Account</Nav.Link>
                            <Nav.Link as={Link} to="/Auth">Auth</Nav.Link>
                        </Nav>                        
                    </Navbar.Collapse>
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-info">Search</Button>
                    </Form>
                </div>
            </Navbar>
        </div>
    )
}

export default Menu
