import React, { Component } from "react";
import "./App.css";
import Main from "./components/Main";
import Menu from "./components/Menu";
import Home from "./components/Home/Home";
import View from "./components/View/View";
import Video from "./components/Video/Video";
import Accounts from "./components/Accounts/Accounts";
import Auth from "./components/Auth/Auth";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Welcome from "./components/Auth/Welcome";
import ProtectedRoute from "./components/Auth/ProtectedRoute";

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Menu />
          <Switch>
            <Route path="/" exact component={Main} />
            <Route path="/Home" component={Home} />
            <Route path="/Post/:id" component={View} />
            <Route path="/Video" component={Video} />
            <Route path="/Account" component={Accounts} />
            <Route path="/Auth" component={Auth} />
            <ProtectedRoute path="/Welcome" component={Welcome} />
            <Route
              path="*"
              component={() => (
                <h2 style={{ marginTop: "20px" }}>404 NOT FOUND</h2>
              )}
            />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}
